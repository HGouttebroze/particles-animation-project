import {
  AxesHelper,
  BoxBufferGeometry,
  Mesh,
  MeshNormalMaterial,
  ParametricGeometry,
  PerspectiveCamera,
  Scene,
  WebGLRenderer,
} from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import "./style.css";

/************ CREATE SCENE ***********/
const scene = new Scene();

// create first element
scene.add(new AxesHelper()); // let create axes, la valeur de l'heler correspond à la la taille de l'axe, en mettant 10 par ex. ça aggrandi la taille des axes

/************ CREATE CAMERA ***********/
const camera = new PerspectiveCamera(
  75,
  window.innerWidth / window.innerHeight,
  0.01,
  1000
); // create first camera

camera.position.z = 2; //on recule le camera vers ns
camera.position.y = 0.5;
camera.position.x = 0.5;
scene.add(camera); //on ajoute la camera a notre scene

/************ CREATE A CUBE WITH "METSH" OBJECT ***********/
// creation d'1 cube à l'aide de l'obet "Mesh"
const cube = new Mesh(
  new BoxBufferGeometry(1, 1, 1), // params: long.larg. prof.
  new MeshNormalMaterial() // material utilisé pr le débug, gére la couleur quand le cube est face caméra... et ces changements...pr bien voir les volumes & tester tt ça
);
scene.add(cube); // j'affiche mon cube

/************ RENDER SCENE TO WINDOW ***********/
// on rend la scene à l'écran
const renderer = new WebGLRenderer({
  antialias: true, // augmente la qualité, attention car ça a 1 coût sur les performances
});
renderer.setSize(window.innerWidth, window.innerHeight); // on definie la taille du rendu
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2)); // def du pixel ratio (! si ecran 4k...si la densité de pixel est > à 1), je limite le ratio à 2 pr économiser des ressources
document.body.appendChild(renderer.domElement); // on rend l'element

/************ IMPORT EXEMPLE ***********/
const controls = new OrbitControls(camera, renderer.domElement); //domElement est pris en second ParametricGeometry, c'est l'element sur lequel on va faire les actions

function tick() {
  // permet de lancement d'une fonction lors d'1 nouveau rendu
  renderer.render(scene, camera); // on cree notre rendur par apport au rendu de cette camera, ns avons l'axe des y en vert et des x en rouge, le z est en bleu (ms on ne le voie pas car il pointe vers la caméra)
  controls.update(); // on met à jour le controle (de l'objet OrbitControls)
  //camera.position.x += 0.01; // par ex., je peux faire evoluer la position de la caméra, au rendu ça fait bouger sur l'axe x, vers la gauche
  //camera.lookAt(0, 0, 0); // pour regarder le centre de la scene, au rendu, s'éloigne et se tourne 1 peu
  requestAnimationFrame(tick); // je rappelle la fonction tick pr avoir des rendus en permanence
}

tick(); // j'appel la fonction au moment du 1er rendu

/* là, on va chercher des infos pour que la scene s'adapte suivant l'écran du user */
window.addEventListener("resize", () => {
  // on écoute avec "resize" et on lance 1 fonction
  camera.aspect = window.innerWidth / window.innerHeight; // je change l'aspect de la caméra
  camera.updateProjectionMatrix(); // et je demande de mettre à jour la caméra
  renderer.setSize(window.innerWidth, window.innerHeight); // je change enfin la taille du renderer
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2)); // et j change aussi le pixel ratio, si changement de taille d'écran
  // mon canevas se redimentionne en fonction de la taille de la fenêtre
});
