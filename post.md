# Create animations with Three.js JavaScript librairie

Three.js est une librairie JavaScript permettant de gérer des objets en 3D. Nous allons créer une petite animation afin d'appréhender cette librairie : des sphères en mouvements.

Mon but est de comprendre le fonctionnement de Three.js, je partagerais plus loin toutes les ressources qui m'auront été utile.

Enfin, le code source sera bien entendu disponible.

## Init project

We'll use "yarn" along this project and "ViteJS" bundle.

1. To create the application, execute `yarn create @vitejs/app`, and we 'll choose JavaScript vanilla's option.

2. Add "three.js" dependencie with `yarn add three`.

3. Launch dev server with `yarn dev`, our app will run on port 3000 by default.

We can find a `main.js` file in .src, we can see that htis file import our CSS style, next, it select the app id and inject our first HTML (a h1 and a link) with the innerHTML methode, using template strings (from ES6). This is the start point of our project.

We will delete this and just keep the CSS import in first ligne.

Pour travailler avec ThreeJS, il faut pense comme sur un logiciel de 3D.
Nous allons tout d'abord créer une scène qui auva des objets, ainsi qu'une caméra qui aura ces objets.

`const scene = new scene`

## ressources help

offical documentation for "points" objects: https://threejs.org/docs/index.html?q=points#api/en/objects/Points

## Particule effect

Pour créer mon effet de particules, je vais créer une multitude d'élément que je place aléatoirement dans un espace donné.
On a plusieurs choix pour créer ces éléments, plus ou moins difficiles:

- on peut créer des sphères, mais ça risque d'être très compliqué, il faut gérer la complexité de la géométrie de la sphère...
- on peut sinon créer des sprites, un plan, qui va toujours faire face à la caméra et où on va créer des PNG, ce seront les particules
